const canvas = document.getElementById('canvas');
const c = canvas.getContext('2d');

const random = (min, max) => { //рандом с произвольными значениями
    return Math.floor(Math.random() * (max - min + 1) + min);
};

const getDistance = (ball1, ball2) => { // находим расстояние между объектами
    return Math.sqrt(Math.pow(ball1.center.x - ball2.center.x, 2) + Math.pow(ball1.center.y - ball2.center.y, 2));
};

const ballsStore = [];


class Balls {
    radius = random(10, 30);
    ballColor = `rgb(${random(0, 255)},${random(0, 255)},${random(0, 255)})`;
    speed = random(1, 10);
    directionAngle = random(0, 360); // угол направления

    center = {
        x: 0,
        y: 0,
    };

    direction = {
        x: Math.cos(this.directionAngle) * this.speed,
        y: Math.sin(this.directionAngle) * this.speed,
    };

    drawBalls = () => { //отрисовка шарика
        c.beginPath();
        c.fillStyle = this.ballColor;
        c.strokeStyle = this.ballColor;
        c.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
        c.stroke();
        c.fill();
    };

    getBallsProp = (event) => { // получение стартового положения центра шарика (при создании по клику)
        this.center.x = event.clientX;
        this.center.y = event.clientY;
    };

    moveBalls = () => { // смена положения шарика
        this.center.x += this.direction.x;
        this.center.y -= this.direction.y;
    };

    borderBounce = () => { //Отбивание от стен
        let rad = this.radius;

        this.center.x + rad > canvas.width ? this.center.x = canvas.width - rad : this.center.x;

        if (this.center.x + rad === canvas.width) {
            this.direction.x *= -1;
        }

        this.center.y + rad > canvas.height ? this.center.y = canvas.height - rad : this.center.y;

        if (this.center.y + rad === canvas.height) {
            this.direction.y *= -1;
        }

        this.center.x - rad < 0 ? this.center.x = rad : this.center.x;

        if (this.center.x - rad === 0) {
            this.direction.x *= -1;
        }

        this.center.y - rad < 0 ? this.center.y = rad : this.center.y;

        if (this.center.y - rad === 0) {
            this.direction.y *= -1;
        }

    };

    ballBounce = () => {

        if (ballsStore.length > 1) {

            for (let ball of ballsStore) {
                let distance = getDistance(this, ball);
                let collision = ball.radius + this.radius;

                if (distance <= collision &&
                    distance !== 0) {

                    //угол столкновения по x и y
                    const cosDistance = (ball.center.x - this.center.x) / distance;
                    const sinDistance = (ball.center.y - this.center.y) / distance;

                    //направление движения до и после столкновения для текущего шарика
                    let currDirec = this.direction.x * cosDistance + this.direction.y * sinDistance;
                    let currDirecCol = -this.direction.x * sinDistance + this.direction.y * cosDistance;

                    //направление движения до и после столкновения для других шариков из ballsStore
                    let ballDirec = ball.direction.x * cosDistance + ball.direction.y * sinDistance;
                    let ballDirecCol = -ball.direction.x * sinDistance + ball.direction.y * cosDistance;

                    //предотвращает залипание друг в друге
                    const collisionTime = (this.radius + ball.radius - distance) / (currDirec-ballDirec);

                    this.center.x -= this.direction.x * collisionTime;
                    this.center.y -= this.direction.y * collisionTime;
                    ball.center.x -= ball.direction.x * collisionTime;
                    ball.center.y -= ball.direction.y * collisionTime;

                    //смена направления движения которое было до столкновения
                    let temp = currDirec;
                    currDirec=ballDirec;
                    ballDirec=temp;

                    //угол столкновения после столкновения
                    this.direction.x = currDirec * cosDistance - currDirecCol * sinDistance;
                    this.direction.y = currDirec * sinDistance + currDirecCol * cosDistance;
                    ball.direction.x = ballDirec * cosDistance - ballDirecCol * sinDistance;
                    ball.direction.y = ballDirec * sinDistance + ballDirecCol * cosDistance;

                    //направление движения после столкновения
                    this.center.x += this.direction.x * collisionTime;
                    this.center.y += this.direction.y * collisionTime;
                    ball.center.x += ball.direction.x * collisionTime;
                    ball.center.y += ball.direction.y * collisionTime;

                    //шары обмениваются направлениями, а не подталкивают друг друга
                    this.direction.x = -(this.direction.x + 1/this.radius);
                    this.direction.y = -(this.direction.y + 1/this.radius);
                }

            }

        }

    }

}


const step = () => { // сценарий шага анимации для поля canvas
    c.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);

    ballsStore.forEach(elem => {
        elem.drawBalls();
        elem.moveBalls();
        elem.borderBounce();
        elem.ballBounce();
    });

};

setInterval(step, 50); // анимация

const init = (event) => { // создание нового шарика
    const ball = new Balls();
    ballsStore.push(ball);
    ball.getBallsProp(event);
};

const deleteBalls = (event) => { //Удаление шарика
    let color = c.getImageData(event.clientX - 10, event.clientY - 10, 1, 1).data; //получает цвет под курсором мыши при клике
    let stringColor = `rgb(${color[0]},${color[1]},${color[2]})`; // переводит цвет в строку для сравнения с цветом шарика

    ballsStore.forEach((elem, index, arr) => {

        if (elem.ballColor === stringColor) {
            arr.splice(index, 1);
        }

    });
};

canvas.addEventListener('click', function (event) { //Условие на проверку клика
    let color = c.getImageData(event.clientX - 10, event.clientY - 10, 1, 1).data;

    if (color[0] === 0 && color[1] === 0 && color[2] === 0) {
        init(event);
    } else {
        deleteBalls(event);
    }

});
